import model.Person;
import model.Staff;
import model.Student;

public class Task_53B_40 {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Anh", "Bình Tân");
        Person person2 = new Person("Kha", "Q10");
        System.out.println("person1: " + person1);
        System.out.println("person2: " + person2);

        Student student1 = new Student(person1.getName(), person1.getAddress(), "Java", 1, 5000000);
        Student student2 = new Student(person2.getName(), person2.getAddress(), "Nodejs", 2, 7000000);
        System.out.println("student1: " + student1);
        System.out.println("student2: " + student2);

        Staff staff1 = new Staff(person1.getName(), person1.getAddress(), "FPT", 45000000);
        Staff staff2 = new Staff(person2.getName(), person2.getAddress(), "SG", 75000000);
        System.out.println("staff1: " + staff1);
        System.out.println("staff2: " + staff2);
    }

}

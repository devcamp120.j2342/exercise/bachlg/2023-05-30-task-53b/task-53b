package model;

public class Circle {
    public double getArea() {
        return Math.PI * this.radius;
    }
    public double getRadius() {
        return this.radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Circle[" +
            " radius='" + getRadius() + "'" +
            ", color='" + getColor() + "'" +
            "]";
    }

    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }
    public Circle(double radius) {
        this.radius = radius;
        this.color = "none";
    }

    public Circle() {
    }
    private double radius;
    private String color;
}

import model.Animal;
import model.Cat;
import model.Dog;
import model.Mammal;


public class Task_53B_60 {
    public static void main(String[] args) throws Exception {
        Animal animal1 = new Animal("Vịt");
        Animal animal2 = new Animal("Trâu");
        System.out.println(animal1);
        System.out.println(animal2);

        Mammal mammal1 = new Mammal("Bò");
        Mammal mammal2 = new Mammal("Cừu");
        System.out.println(mammal1);
        System.out.println(mammal2);

        Cat cat1 = new Cat("Tam thể");
        Cat cat2 = new Cat("Mèo mun");
        System.out.println(cat1);
        System.out.println(cat2);
        cat1.greets();
        cat2.greets();

        Dog dog1 = new Dog("Alaska");
        Dog dog2 = new Dog("Husky");
        System.out.println(dog1);
        System.out.println(dog2);
        dog1.greets();
        dog2.greets();
        dog1.greets(dog2);
    }

}

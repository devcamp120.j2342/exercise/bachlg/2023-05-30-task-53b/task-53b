import model.Author;
import model.Book;

public class Task_53B_10 {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Bach", "bachlg@gmail.com", 'm');
        Author author2 = new Author("Gia Bach", "bachlg@devcamp.vn", 'm');
        System.out.println("Author 1: " + author1);
        System.out.println("Author 2: " + author2);

        Book book1 = new Book("Java coding in 24h", author1, 200.103);
        Book book2 = new Book("NodeJS coding in 24h", author2, 300.999, 3);
        System.out.println("Book1: " + book1);
        System.out.println("Book2: " + book2);
    }
}
